\documentclass[../main.tex]{subfiles}
 
\begin{document}

\chapter{Workflow Similarity Analysis} \label{workflow_analysis}
\epigraph{Science cannot solve the ultimate mystery of nature. And that is because, in the last analysis, we ourselves are a part of the mystery that we are trying to solve.}{Max Planck}
\pagebreak

Strokes can relate to one another via contexts, can be globally aligned, or can be random and unpredictable. The system needs to be able to figure
out such potential relationships to provide good predictions and to
adapt the synthesis to the local context variation.


Intuitively, the way to predict a new stroke $op_o$ (including properties
like position, direction, color\ldots etc.) is to apply the drawing
styles of similar strokes drawn before.


The prediction $u(op_o)$ will be the input to the synthesis stage, the properties $u(op_o)$ need to be predicted as the collection of $u(s_o)$ for each $s_o \in op_o$, including spatial, temporal, and appearance information.


\section{Matching Sample Pairs}

Matching sample pairs: are sample pairs that minimizes the total dissimilarities between two matching strokes (determined by the equation of the operations similarities)

\begin{figure}[ht]
\includegraphics{matching_similar_pairs}
\caption{Matching sample pairs of two similar strokes by minimizing the total dissimilarities}
\end{figure}

\section{Two Separate Frames for Workflow Analysis}

Based on the multiple variation of relationships between strokes, two potential frames are considered for each stroke: local (as determined by a nearby context stroke), and global (as determined by the default global coordinate system).Thus the analysis is performed under two frames separately.


\section{Local Analysis}

Finding similarity matches between the last-drawn stroke and previously drawn strokes, with regard to the (shared or individual) context of the strokes. So the way to predict a new stroke is by performing contextual analysis on the set of strokes similar to the last drawn stroke, to derive how they are drawn in their own contexts and utilize such information to predict the properties of the predicted stroke in a new context.

\begin{figure}[ht]
\includegraphics{individual_contexts}
\caption{Individual contexts (the small branches)}
\end{figure}

\begin{figure}[ht]
\includegraphics{shared_context}
\caption{Shared context (the large branch)}
\end{figure}


\subsection{Context Strokes}

Context strokes are nearby long Strokes within the neighborhood of a Stroke. Long as in at least twice the length of the stroke, e.g. long strokes that usually correspond to skeletons or outlines If no such long strokes are around, then the absence of local context is considered and the global analysis below is used.
The goal of defining context strokes is:

\begin{enumerate}
    \item Capturing the control structure of the painting.
    \item Producing an output stroke that has the ability to adapt to its context.
\end{enumerate}

\begin{figure}[ht]
\includegraphics[scale=0.5]{neighbourhood_types}
\caption{Different types of neighbourhood operations around the central red operation. Similar strokes (shown in blue) context stroke (shown in green).}
\end{figure}

\subsection{Matching Local Sample Pairs}

In order to know how a stroke $op_o$ should be constrained by a specific context stroke $op_o$, statistical analysis is performed among the matching set $\{ \hat{u}(op'_i, op_i) \}$ where each $op_i$ is a matching stroke of $op_o$ and $\hat{u}(op'_i, op_i)$ is the matching pair of $\hat{u}(op'_o, op_o)$, Specifically, for each sample-pair $\hat{u}(s'_o, s_o) \in \hat{u}(op'_o, op_o)$, its matching sample-pairs $\{ \hat{u}(s'_i, s_i) \}$ are extracted from $\{ \hat{u}(op'_i, op_i) \}$. Statistically, $\{ \hat{u}(s'_i, s_i) \}$ can be characterized as:


$ (\bar{u}(s'_i, s_i), \sigma(s'_i, s_i)) $


Where $\bar{u}(s'_i, s_i)$ and $\sigma(s'_i, s_i)$ are the mean and standard deviation of the set $\{ \hat{u}(s'_i, s_i) \}$.


\textbf{Mean:} is defined as the average of the set.


$ \bar{u}(s'_i, s_i) = \frac{1}{n} \sum(s'_i, s_i) $


\textbf{Standard Deviation:} is defined as how much the members of a set differ from the mean value for the set.

The $ \bar{u}(s'_i, s_i) $ is used as the prediction for $\hat{u}(s'_o, s_o)$ and $\sigma(s'_i, s_i)$ as the plausibility of the prediction.

\section{Global Similarity Analysis}

Finding similarity matches between the last-drawn stroke and previously drawn strokes, but with absence of a context stroke.

\subsection{Matching Global Matching Pairs}

Similarly to the local analysis (except that there is no context stroke). Specifically, for each sample $s_o \in op_o$, its matching samples $\{ s_i \}$ are extracted among its matching operations $\{ op_i \}$ and compute the mean and deviation of the set $\{ u(s_i) \}$:


$ (\bar{u}(s'_i, s_i), \sigma(s'_i, s_i)) $


Similar to local analysis above, $\bar{u}(s'_i, s_i)$ is the mean of the set $\{ u(s_i) \}$ and provides a prediction for $ u(s_o) $ and $\sigma(s_i)$ is the standard deviation for the set $\{ u(s_i) \}$ and indicates the plausibility of the prediction.


\section{Plausibility of the Prediction}

From local and global analysis, two separate predictions are derived. However, prediction quality may vary from case to case depending on whether the contexts are local or global and if structure, color, or some other combinations of properties are involved. Accordingly to above observation, each property (e.g.direction and color) is considered separately. For a particular parameter $u_i(s_o) \in u(s_o)$, the quality of its prediction $\bar{u}(s_o)$ (both for local and global analysis) is formulated as:


$Q(\bar{u}(s_o)) = exp(-\frac{\sigma(u_i(s_o))}{\sigma_1})$


Where $\sigma_1$ is parameter dependent on the specific $u_i(s_o) \in u_i(s_o)$ In particular, if the size of matching set $\{ \hat{u}(op'_i, op_i) \}$ is small (less than 3), then such prediction is considered not reliable, and each $ Q(\bar{u}(s_o)) $ is set to be equal 0.


\section{Prediction}

By combining all predictions (local prediction for each context operation and global prediction), the final prediction can be calculated by minimizing the following energy function:


\begin{align}
    E(u(op_o)) = & \underset{s_o \in op_o}{\sum} \underset{op'_o \in \{op'_o\}}{\sum} \\
    & Q(\bar{u}(s_o))|u(s_o) - \bar{u}(s_o)|^2 \\
    & + Q(\bar{u}(s_o, s'_o))| u(s_o) - u(s'_o) - \bar{u}(s_o, s'_o) |^2
\end{align}


Where $s'_o \in op'_o$ is the matching sample of $s_o \in op_o$ and each prediction is weighted by their prediction quality.


\end{document}
