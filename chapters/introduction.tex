\documentclass[../main.tex]{subfiles}
 
\begin{document}

\chapter{Introduction} \label{Introduction}
\epigraph{Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do. If you haven't found it yet, keep looking. Don't settle. As with all matters of the heart, you'll know when you find it.}{Steve Jobs}
\pagebreak

\section{Autocomplete Painting}
Painting is a common practice for content creation, often involving
repetitive implements such as stipples, hatches, or brushes. Such
repetitions are an integral part of artistic styles and compositions.
Yet, they can be quite tedious for manual labor and too versatile for
fully automatic generation.


A central goal of digital painting research is to automate such repetitions while allowing sufficient control and expressive power for
users [\cite{winkenbach_1994}]. This has been greatly advanced in recent data driven methods (e.g. [\cite{kazi_2012}; \cite{Lu_2013}; \cite{Lukac2013}]), but they impose a batch/sequential
order, in which the desired repetitions have to be prepared first 
(usually in the form of small exemplars) and then cloned to the desired
output regions through various control gestures. Such sequential
mode may break the continuous and spontaneous nature of painting flows. For example, users might not know a priori the desired
repetitions and would prefer to experiment on the go. Moreover,
such batch control is most suitable for large, homogeneous patterned regions. More nuanced or detailed variations, as common
in real paintings, can require an excessive amount of exemplars and
gestures. Thus, it can be more natural and effective for users to
draw as usual, while having the system automatically detect and
assist potential repetitions.


The project presents a UI system that uses past workflows to facilitate interactive authoring of future repetitions. Users can paint with the system as usual similar to ordinary digital painting tools.
Meanwhile, the system gradually records their past workflows and
analyzes their structure and color relationships. With enough repetition detected, it can predict and autocomplete what the users
might want to draw in the near future (temporal ordering), around
the current drawing region (spatial proximity), or across similarly
colored or structured regions (contextual similarity). The method
eliminates the need for sequential clone and better adapts to local
painting variations. Furthermore, users can accept, ignore, or modify the autocompletes and thus maintain full control.
Autocomplete has been a core feature for common text editing and
programming IDE systems, for which the system can be considered
as a painting analogy. The key idea is to treat workflow repetition as
a form of texture, and extend texture analysis and synthesis methods
[Wei et al. 2009; Ma et al. 2011] to provide a predictive interface for
sketching [Lee et al. 2011; Fernquist et al. 2011]. Workflows have
been found useful for a variety of applications [Grossman et al.
2010; Denning et al. 2011; Chen et al. 2011; Zitnick 2013]. In the
method, the project uses workflows for both predicting future drawings and
adapting them to the surrounding color and shape contexts.
The key challenge is to extend and integrate these methods for quality results and fluid interactions. The project performs contextual analysis
[Lu et al. 2007; Guerrero et al. 2014] on the shape and color relationships among prior painting operations, from which the project derives
predictions for potential future ones. These predictions serve as
extra constraints in a texture optimization framework [Ma et al.
2011; Ma et al. 2013], and help maintain nuanced contextual relationships in synthesis. Moreover, these analysis and synthesis
are conducted incrementally around the spatial-temporal vicinity of
the current operation, and thus sufficiently efficient for interactive
painting. Unlike prior constrained drawing systems (e.g. [Maulsby
et al. 1989; Gleicher and Witkin 1994]) that require explicit user
specification, the method automatically detects potential relationships in the background.


The project evaluates the method via drawing results and a pilot user study.
In sum, the project has the following contributions:
\begin{itemize}
 \item A system to facilitate interactive painting of common repetitions such as stipples and hatches.
 \item A predictive user interface design that observes ordinary
painting flows while reducing tedious manual labor.
 \item Algorithms to analyze painting workflows and to synthesize
new results with high quality and speed.
\end{itemize}

\section{Traditional Animation History}
2D Animation is the process of making hundreds of drawings and then having them animate by playing them in rapid succession. This is the most common form of animation, and has been used for several decades across several forms of media.


Traditional 2D animation involves making a drawing and then making a second drawing of the same thing, but with a very subtle change (such as a closed mouth to an open mouth or a person taking one footstep). Hundreds, if not thousands, of unique drawings are made that go from the beginning of a scene to the end of a scene. The drawings are then shown in very rapid succession (typically 24 frames, or unique drawings, per second). This gives the illusion that the numerous still images form a complete, fluid animation.

\subsection{Flip Book in 1868}
John Barnes Linnett patented the first flip book in 1868 as the kineograph. A flip book is a small book with relatively springy pages, each having one in a series of animation images located near its unbound edge. The user bends all of the pages back, normally with the thumb, then by a gradual motion of the hand allows them to spring free one at a time. As with the phenakistoscope, zoetrope and praxinoscope, the illusion of motion is created by the apparent sudden replacement of each image by the next in the series, but unlike those other inventions no view-interrupting shutter or assembly of mirrors is required and no viewing device other than the user's hand is absolutely necessary. Early film animators cited flip books as their inspiration more often than the earlier devices, which did not reach as wide an audience.
The older devices by their nature severely limit the number of images that can be included in a sequence without making the device very large or the images impractically small. The book format still imposes a physical limit, but many dozens of images of ample size can easily be accommodated. Inventors stretched even that limit with the mutoscope, patented in 1894 and sometimes still found in amusement arcades. It consists of a large circularly-bound flip book in a housing, with a viewing lens and a crank handle that drives a mechanism that slowly rotates the assembly of images past a catch, sized to match the running time of an entire reel of film.

\includegraphics[scale=4]{flipbook}

\subsection{Traditional Animation}

The first film recorded on standard picture film that included animated sequences was the 1900 Enchanted Drawing, which was followed by the first entirely animated film, the 1906 Humorous Phases of Funny Faces by J. Stuart Blackton—who is, for this reason, considered the father of American animation.
 
In Europe, the French artist, Émile Cohl, created the first animated film using what came to be known as traditional animation creation methods—the 1908 Fantasmagorie. The film largely consisted of a stick figure moving about and encountering all manner of morphing objects, such as a wine bottle that transforms into a flower. There were also sections of live action where the animator’s hands would enter the scene. The film was created by drawing each frame on paper and then shooting each frame onto negative film, which gave the picture a blackboard look.


The more detailed hand-drawn animations, requiring a team of animators drawing each frame manually with detailed backgrounds and characters, were those directed by Winsor McCay, a successful newspaper cartoonist, including the 1911 Little Nemo, the 1914 Gertie the Dinosaur, and the 1918 The Sinking of the Lusitania.


During the 1910s, the production of animated short films, typically referred to as "cartoons", became an industry of its own and cartoon shorts were produced for showing in movie theaters. The most successful producer at the time was John Randolph Bray, who, along with animator Earl Hurd, patented the cel animation process that dominated the animation industry for the rest of the decade.


\includegraphics{cartoon}

\subsection{Walt Disney \& Warner Bros.}

In 1923, a studio called Laugh-O-Grams went bankrupt and its owner, Walt Disney, opened a new studio in Los Angeles. Disney's first project was the Alice Comedies series, which featured a live action girl interacting with numerous cartoon characters. Disney's first notable breakthrough was 1928's Steamboat Willie, the third of the Mickey Mouse series.It was the first cartoon that included a fully post-produced soundtrack, featuring voice and sound effects printed on the film itself ("sound-on-film"). The short film showed an anthropomorphic mouse named Mickey neglecting his work on a steamboat to instead make music using the animals aboard the boat.
In 1933, Warner Brothers Cartoons was founded. While Disney's studio was known for its releases being strictly controlled by Walt Disney himself, Warner brothers allowed its animators more freedom, which allowed for their animators to develop more recognizable personal styles.


The first animation to use the full, three-color Technicolor method was Flowers and Trees, made in 1932 by Disney Studios, which won an Academy Award for the work. Color animation soon became the industry standard, and in 1934, Warner Brothers released Honeymoon Hotel of the Merrie Melodies series, their first color films. Meanwhile, Disney had realized that the success of animated films depended upon telling emotionally gripping stories; he developed an innovation called a "story department" where storyboard artists separate from the animators would focus on story development alone, which proved its worth when the Disney studio released in 1933 the first-ever animated short to feature well-developed characters, Three Little Pigs.In 1935, Tex Avery released his first film with Warner Brothers. Avery's style was notably fast paced, violent, and satirical, with a slapstick sensibility.

\section{Auto-complete Animations}

Hand-drawn animation is a popular art form and communication
medium. However, it is challenging to produce, even for experienced professionals. In addition to appropriate spatial arrangement
in one frame, users also need to maintain a consistent temporal
flow across multiple frames. Existing computer graphics methods
can ameliorate this difficulty to some extent by reusing underlying art content, such as deforming shape templates [Igarashi et al.
2005] or cloning animated texture sprites [\cite{kazi_2012}]. However, manual drawing can provide unique freedom of expression
and more natural touch for many artists. Thus, there is a pressing
need of interactive tools that can support creation of manual animation more effectively while maintaining artists’ natural drawing
practices. This demand is not only for professionals due to the rise
of mobile and social applications for authoring and sharing hand drawn animations, which are used by many amateur artists.
We present an interactive drawing system that helps users produce
animation more easily and in a better quality while preserving manual drawing practices. Users simply create a series of drawings in
our system as they would do in their common tools. Our system
records and analyzes their past drawings in the background, and
provides suggestions that can save manual labor and improve drawing quality. As illustrated in Figure 1, the system can detect potential repetitions such as static objects and dynamic motions, and
predict what might need to be drawn across spatial locations and
temporal frames. Users can accept, ignore, or modify such suggestions analogous to the mechanisms for auto-complete spatial repetitions for single static sketches in [\cite{painting_repetition}]. Later, the
users may want to modify existing animations, such as changing
the shapes or colors of certain objects. They can simply make desired visual changes in one frame, and our system will propagate
these changes across similar spatial objects at all temporal frames
to reduce tedious manual repetitions. Unlike many existing deformation methods, our system allows users to create animations with
topological changes, such as breaking objects or growing plants.
The key idea of this work is to extend the analysis and synthesis
algorithms of drawing repetitions within individual frames [\cite{painting_repetition}] to multiple frames of animations. The method in [\cite{painting_repetition}] is based on local similarity, which can predict only low level spatial repetitions such as hatches, but not high-level structures such as complex objects in animations. To overcome this challenge, we extend their local similarity to a global similarity that can
capture both global contexts (e.g., object contours) and local details
(e.g., individual strokes). We then measure the global similarity
between past and current drawings to predict what the users might
want to draw in the future. The prediction is not 100\% accurate, but
is robust enough for common variations (including number, shape,
and order of strokes) as long as the drawing is reasonably coherent
across frames, such as when people follow the well-known blocking technique [Iarussi et al. 2013]. Our preliminary user study confirmed that participants were able to produce a wide variety of animations in our system with a reduced number of strokes and in
good quality, and expressed positive experiences.

\section{Limitations}

\section{Traditional System Limitations}

\begin{itemize}
 \item Time Consuming
 
 Animators draw individual frames for each scene. The large numbers of drawings and the photography time required in completing a production consumes schedules and can result in delays. Reliance of a large crew of animators makes it difficult to speed up production as each animator works at a different speed. Traditional animation takes more time compared to computer animation, which uses animation software to speed up the process.
 
 \item Correcting Mistakes
 
 A mistake in traditional animation requires you to repeat the whole drawing instead of deleting and correcting a mistake. The repetition of work can become tiresome and time consuming. Artistic corrections for quality animations are costly, as they demand repetition of an entire task. Repetition of tasks to correct mistakes involves a large crew incurring the production more costs.
 
 \item Costs
 
 Traditional animation requires a numbers of tools and equipment per production. They include drawing tools, tracing tools, and photographic equipment. It also requires a large crew to draw the characters, draw backgrounds and shoot photos of the final images.

\end{itemize}

\section{Skeletal System Limitations}

Skeletal animation or bone-based animation is a technique in computer animation in which the object being animated has two main components: a surface representation used to draw the object (skin, mesh, character) and set of interconnected bones used to animate the surface (skeleton, rig, set of bones).

\begin{itemize}
 \item Bone Rigidity
 
 Bones cannot be moved relative to each other.
 
 \item Single Parent Restriction
 
 A bone can have only one parent, so meshes of bones cannot be created.
 
 \item Hierarchical Structure of the Skeleton
 
 Not suitable for animating liquids or gasses due its complexity.
\end{itemize}


\includegraphics[scale=0.5]{rigging}

\section{Motivation}

Auto-complete hand drawn animation is the first free and open system that provide the feature of detect patterns and predict the future drawn that the user going to draw, the existing painting digital software -close source or even open source- like Pencil2D or OpenToonz does not offer this mechanism, so it was going to be a great success for us to help solving a problem like that which face artists in drawing high detailed drawn repetitions.

\section{Problem Definition}

there is no digital painting systems that facilitate the repetitive operations and automate it to allow the user to focus on the creative process only and control, so artist or user has to repeat each high detailed drown again each time he needs it. the same problem face artist or users in digital painting animation systems also which cause a great loss in efforts and time, and that was the start of building our graduation project that predict what's next and what the user going to draw in future with availability of accept or ignore these predicts.

\section{Document Outline}

Auto-Complete Hand Drawn project specified these next chapters including all details about the project.

\subsection{Introduction}

Stating the problem definition, motivations, variety of digital painting systems,
limitations, for making this project, for the project and the history of speech,
Recognition.

\subsection{Background and Previous Work}

In this section we we talk about the different concepts and fields that are required as basis to have a good understanding the project. We discuss how we define a stroke, and what fields of computer vision, computer graphics and linear algebra used in our project.

\subsection{System Design}

The system design talks about how our system is laid out and how the data flows from one part of the system to the other.

\subsection{Similarity Measures}

Here we discuss the properties of the point, stroke and neighbours that we take into account when we compare them to each other detecting the similarity or the lack of it.

\subsection{Neighborhood Similarity}

We discuss in details the means by which we go about comparing the neighbourhoods of two points and/or two strokes. We also discuss the algorithms used and how efficient are they.

\subsection{Synthesis}

Synthesis is the process of utilizing the similarity analysis functions in order to produce a prediction based on the past drawings of the user.

\subsection{Workflow Analysis}

The process of analyzing the strokes and their contexts in order to produce a prediction that it context aware and plausible.

\subsection{Implementation}

The implementation chapter takes about the implementation details. The functions, the constants, the structs that we use and the typedefs. It's basically code documentation.

\subsection{Tools}

The tools we used in our project and the ones used in OpenToonz. How useful they are. And why they were chosen.

\subsection{Limitations and Future Works}

No work is perfect work. We discuss how the ideas that we believe would make the project better and more usable to artists and students all around the world.

\end{document}
