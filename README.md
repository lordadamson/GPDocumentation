# GPDocumentation
Documentation for my GP in LaTeX 

## dependencies
I recommend you install the whole latex thing. But here are the packages I use just in case.
* subfiles
* biblatex (biber as the backed) for the references
* csquotes
* color
* hyperref
* graphicx
* amsmath
* epigraph
* subfigure
* cleveref
* commath
